jQuery(document).ready(function() {
    jQuery('select').css('color','#a3a3a3');
    jQuery('select').change(function() {
       var current = jQuery(this).val();
       if (current != 'null') {
           jQuery(this).css('color','#000');
       } else {
           jQuery(this).css('color','#a3a3a3');
       }
    });  
});


jQuery(function() {
      jQuery('[data-toggle="datepicker"]').datepicker({
        autoHide: true,
        zIndex: 2048,
      });
    });



//upload file

//Reference: 
//https://www.onextrapixel.com/2012/12/10/how-to-create-a-custom-file-input-with-jquery-css3-and-php/
;(function($) {

		  // Browser supports HTML5 multiple file?
		  var multipleSupport = typeof $('<input/>')[0].multiple !== 'undefined',
		      isIE = /msie/i.test( navigator.userAgent );

		  $.fn.customFile = function() {

		    return this.each(function() {

		      var $file = $(this).addClass('custom-file-upload-hidden'), // the original file input
		          $wrap = $('<div class="form-group upload_filed">'),
		          $input = $('<input type="text" class=" form-control attached__field" placeholder="Upload File"/>'),
		          // Button that will be used in non-IE browsers
		          $button = $('<button type="button" class="file-upload-button attached_icon"></button>'),
		          // Hack for IE
		          $label = $('<label class="file-upload-button" for="'+ $file[0].id +'">Select a File</label>');

		      // Hide by shifting to the left so we
		      // can still trigger events
		      $file.css({
		        position: 'absolute',
		        left: '-9999px'
		      });

		      $wrap.insertAfter( $file )
		        .append( $file, $input, ( isIE ? $label : $button ) );

		      // Prevent focus
		      $file.attr('tabIndex', -1);
		      $button.attr('tabIndex', -1);

		      $button.click(function () {
		        $file.focus().click(); // Open dialog
		      });

		      $file.change(function() {

		        var files = [], fileArr, filename;

		        // If multiple is supported then extract
		        // all filenames from the file array
		        if ( multipleSupport ) {
		          fileArr = $file[0].files;
		          for ( var i = 0, len = fileArr.length; i < len; i++ ) {
		            files.push( fileArr[i].name );
		          }
		          filename = files.join(', ');

		        // If not supported then just take the value
		        // and remove the path to just show the filename
		        } else {
		          filename = $file.val().split('\\').pop();
		        }

		        $input.val( filename ) // Set the value
		          .attr('title', filename) // Show filename in title tootlip
		          .focus(); // Regain focus

		      });

		      $input.on({
		        blur: function() { $file.trigger('blur'); },
		        keydown: function( e ) {
		          if ( e.which === 13 ) { // Enter
		            if ( !isIE ) { $file.trigger('click'); }
		          } else if ( e.which === 8 || e.which === 46 ) { // Backspace & Del
		            // On some browsers the value is read-only
		            // with this trick we remove the old input and add
		            // a clean clone with all the original events attached
		            $file.replaceWith( $file = $file.clone( true ) );
		            $file.trigger('change');
		            $input.val('');
		          } else if ( e.which === 9 ){ // TAB
		            return;
		          } else { // All other keys
		            return false;
		          }
		        }
		      });

		    });

		  };

		  // Old browser fallback
		  if ( !multipleSupport ) {
		    $( document ).on('change', 'input.customfile', function() {

		      var $this = $(this),
		          // Create a unique ID so we
		          // can attach the label to the input
		          uniqId = 'customfile_'+ (new Date()).getTime(),
		          $wrap = $this.parent(),

		          // Filter empty input
		          $inputs = $wrap.siblings().find('.file-upload-input')
		            .filter(function(){ return !this.value }),

		          $file = $('<input type="file" id="'+ uniqId +'" name="'+ $this.attr('name') +'"/>');

		      // 1ms timeout so it runs after all other events
		      // that modify the value have triggered
		      setTimeout(function() {
		        // Add a new input
		        if ( $this.val() ) {
		          // Check for empty fields to prevent
		          // creating new inputs when changing files
		          if ( !$inputs.length ) {
		            $wrap.after( $file );
		            $file.customFile();
		          }
		        // Remove and reorganize inputs
		        } else {
		          $inputs.parent().remove();
		          // Move the input so it's always last on the list
		          $wrap.appendTo( $wrap.parent() );
		          $wrap.find('input').focus();
		        }
		      }, 1);

		    });
		  }

}(jQuery));

jQuery('input[type=file].form-control').customFile();



/*upload img*/



jQuery(document).ready(function() {
jQuery(".profileBox input:file").on('change', function (){
		var fileName = jQuery(this).val();
		if(fileName.length >0){
    //jQuery(this).parent().children('span').html(fileName);
		}
		else{
			//jQuery(this).parent().children('span').html("Choose file");

		}
	});
	//file input preview
	function readURL(input) { 
		if (input.files && input.files[0]) {
				var reader = new FileReader();
				reader.onload = function (e) {
						jQuery('.profileBox figure img').attr('src', e.target.result);
				}
				reader.readAsDataURL(input.files[0]);
		}
	}
	jQuery('body').find(".profileBox .fileContainer input[type='file']").on('change',function(){
			readURL(this);
	});

});


//custom scroll//

(function(jQuery){
			jQuery(window).on("load",function(){
				
				jQuery(".dashboard__menulist").mCustomScrollbar({
					theme:"minimal"
				});
                
               jQuery(".dash__rt__inn_main").mCustomScrollbar({
					theme:"minimal"
				});
                
               jQuery(".dash__rt__inn_main .table__bx").mCustomScrollbar({
                    axis:"x",
					theme:"minimal"
				});
                
              
				
			});
		})(jQuery); 









